# Employees Management Application

### Prerequisites

- [Docker 19.03.13](https://www.docker.com/get-started)

## Installation

_1. Download the project to your local machine_

_2. Inside of the project folder run the following command_

```
$ docker-compose up -d
```

## Running PHPUnit to execute tests

_1. Run the following command to access the docker container application_

```
$ docker exec -it employees-app bash
```

_2. Inside the docker container run the command:_

```
$ vendor/bin/phpunit
```

## Technologies & Languages

**Laravel 8.20.1**

**PHP 7.4.3**

**Docker 19.03.13**

**Nginx 1.15.0**

**Version control (Git)** [https://git-scm.com/](https://git-scm.com/)

