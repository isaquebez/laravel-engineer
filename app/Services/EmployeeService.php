<?php

namespace App\Services;

use App\Jobs\SendEmployeesImportEmailJob;
use App\Models\Employee;
use App\Repositories\EmployeeRepository;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Validator;
use InvalidArgumentException;

class EmployeeService
{
    protected EmployeeRepository $employeeRepository;

    /**
     * EmployeeService constructor.
     *
     * @param EmployeeRepository $employeeRepository
     */
    public function __construct(EmployeeRepository $employeeRepository)
    {
        $this->employeeRepository = $employeeRepository;
    }

    /**
     * Get all employee.
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->employeeRepository->getAll();
    }

    /**
     * Get all employee by user.
     *
     * @return Collection
     */
    public function getAllByUser(): Collection
    {
        return $this->employeeRepository->getAllByUser();
    }

    /**
     * Save all employees from an array
     *
     * @param array $employeesArr
     * @return Response
     */
    public function saveEmployeesFromArray(array $employeesArr): Response
    {
        /**
         * Begin a DB transaction
         */
        DB::beginTransaction();

        try
        {
            /**
             * Try to save all employees into DB
             */
            for ($i = 0; $i < count($employeesArr); $i++)
            {
                $this->updateOrCreateEmployeeData($employeesArr[$i]);
            }

            /**
             * Commit transaction
             */
            DB::commit();
        } catch (\Exception $e) {
            /**
             * Rollback when an error occurs
             */
            DB::rollback();

            /**
             * Set the response data in invalid date cases
             */
            if ($e->getMessage() == Lang::get('validation.date', ['attribute' => 'start date'])) {
                return response([
                    'message' => 'Invalid Date'
                ], Response::HTTP_NOT_ACCEPTABLE);
            }

            return response([
                'message' => $e->getMessage()
            ], 400);
        }

        $details['email'] = auth()->user()->email;

        dispatch(new SendEmployeesImportEmailJob($details));

        /**
         * Set the response data in success cases
         */
        return response([
            'message' => 'success'
        ], Response::HTTP_OK);
    }

    /**
     * Store to DB if there are no errors.
     *
     * @param array $data
     * @return Employee
     */
    public function updateOrCreateEmployeeData(array $data): Employee
    {
        $validator = $this->validateEmployeeData($data);

        if ($validator->fails()) {
            throw new InvalidArgumentException($validator->errors()->first());
        }

        return $this->employeeRepository->updateOrCreate($data);
    }

    /**
     * Validate employee data.
     * @param $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function validateEmployeeData($data): \Illuminate\Contracts\Validation\Validator
    {
        return Validator::make($data, [
            'name' => 'required',
            'email' => 'required',
            'document' => 'required',
            'city' => 'required',
            'state' => 'required',
            'start_date' => 'required|date|date_format:Y-m-d',
        ]);
    }
}
