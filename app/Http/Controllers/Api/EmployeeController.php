<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Employee;
use App\Services\EmployeeService;
use App\Traits\CsvData;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class EmployeeController extends Controller
{
    use CsvData;

    protected EmployeeService $employeeService;

    /**
     * EmployeeController Constructor
     *
     * @param EmployeeService $employeeService
     *
     */
    public function __construct(EmployeeService $employeeService)
    {
        $this->employeeService = $employeeService;
    }

    /**
     * Get all employees from user
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(
            $this->employeeService->getAllByUser()
        );
    }

    /**
     * Store all employees from an csv file
     * @param Request $request
     * @return Response
     */
    public function storeFromCsv(Request $request): Response
    {
        /**
         * Get the CSV file
         */
        $file = $request->file('employees');

        /**
         * Convert the CSV to array
         */
        $employeesArr = $this->csvToArray($file);

        /**
         * Try to save all employees into DB and returns response
         */
        return $this->employeeService->saveEmployeesFromArray($employeesArr);
    }


    public function show(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json(
            $employee
        );
    }

    public function destroy(Employee $employee): JsonResponse
    {
        $this->authorize('employee', $employee);

        return response()->json([
            'Success' => $employee->delete()
        ]);
    }
}
