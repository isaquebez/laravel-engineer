<?php

namespace App\Repositories;

use App\Models\Employee;
use Illuminate\Database\Eloquent\Collection;

class EmployeeRepository
{
    protected Employee $employee;

    /**
     * EmployeeRepository constructor.
     *
     * @param Employee $employee
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * Get all employees.
     *
     * @return Collection
     */
    public function getAll(): Collection
    {
        return $this->employee->get();
    }

    /**
     * Get all employees by user.
     *
     * @return Collection
     */
    public function getAllByUser(): Collection
    {
        return $this->employee->where('user_id', auth()->user()->id)->get();
    }

    /**
     * Create or update the employee
     *
     * @param array $data
     * @return mixed
     */
    public function updateOrCreate(array $data)
    {
        $employee = Employee::updateOrCreate(['email' => $data['email']], [
            'user_id' => auth()->user()->id,
            'name' => $data['name'],
            'email' => $data['email'],
            'document' => $data['document'],
            'city' => $data['city'],
            'state' => $data['state'],
            'start_date' => $data['start_date'],
        ]);

        return $employee->fresh();
    }

    /**
     * Delete Employee
     *
     * @param $id
     */
    public function delete($id)
    {
        $employee = $this->employee->find($id);
        $employee->delete();
    }
}
