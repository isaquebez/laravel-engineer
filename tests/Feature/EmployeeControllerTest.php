<?php

namespace Tests\Feature;

use App\Models\Employee;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Http\File;
use Illuminate\Http\UploadedFile;
use Laravel\Passport\Passport;
use Tests\TestCase;

class EmployeeControllerTest extends TestCase
{
    use DatabaseMigrations;

    protected User $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = Passport::actingAs(
            User::factory()->create(),
            ['create-servers']
        );
    }

    public function test_it_gets_all_employees_with_a_auth_user()
    {
        $employees = Employee::factory(3)->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.index'))
            ->assertOk()
            ->assertJson(
                $employees->map(function ($employee) {
                    return [
                        'id' => $employee->id,
                        'user_id' => $employee->user_id,
                        'name' => $employee->name,
                        'email' => $employee->email,
                        'document' => $employee->document,
                        'city' => $employee->city,
                        'state' => $employee->state,
                        'start_date' => $employee->start_date,
                    ];
                })->toArray()
            );
    }

    public function test_it_cannot_gets_employees_from_another_user()
    {
        $user = User::factory()->create();

        Employee::factory(3)->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.index'))
            ->assertOk()
            ->assertExactJson([]);
    }

    public function test_it_shows_a_employee_with_a_auth_user()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->get(route('employees.show', [$employee->id]),)
            ->assertOk()
            ->assertJson([
                'id' => $employee->id,
                'user_id' => $employee->user_id,
                'name' => $employee->name,
                'email' => $employee->email,
                'document' => $employee->document,
                'city' => $employee->city,
                'state' => $employee->state,
                'start_date' => $employee->start_date,
            ]);
    }

    public function test_it_cannot_shows_a_employee_from_another_user()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->get(route('employees.show', [$employee->id]))
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');
    }

    public function test_it_deletes_a_employee_with_a_auth_user()
    {
        $employee = Employee::factory()->create([
            'user_id' => $this->user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]),)
            ->assertOk()
            ->assertExactJson([
                'Success' => true
            ]);

        $this->assertDatabaseMissing('employees', [
            'id' => $employee->id
        ]);
    }

    public function test_it_cannot_deletes_a_employee_from_another_user()
    {
        $user = User::factory()->create();

        $employee = Employee::factory()->create([
            'user_id' => $user->id
        ]);

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);

        $this->delete(route('employees.destroy', [$employee->id]),)
            ->assertForbidden()
            ->assertSee('This action is unauthorized.');

        $this->assertDatabaseHas('employees', [
            'id' => $employee->id,
            'user_id' => $user->id
        ]);
    }

    public function test_employees_import_from_valid_csv()
    {
        $csv = $this->getFile('app/tests/valid-csv.csv');

        $this->json('POST', route('employees.store.from.csv'), [
            'employees' => $csv,
        ])->assertStatus(200);
    }

    public function test_it_cannot_import_csv_with_invalid_data()
    {
        $csv = $this->getFile('app/tests/invalid-csv.csv');

        $this->json('POST', route('employees.store.from.csv'), [
            'employees' => $csv,
        ])->assertStatus(500);
    }

    public function test_it_can_update_employees_via_csv()
    {
        $csv = $this->getFile('app/tests/valid-csv.csv');

        $this->json('POST', route('employees.store.from.csv'), [
            'employees' => $csv,
        ])->assertStatus(200);

        $updateCsv = $this->getFile('app/tests/update-csv.csv');

        $this->json('POST', route('employees.store.from.csv'), [
            'employees' => $updateCsv,
        ])->assertStatus(200);

        $employeeNewState = Employee::where('email', 'marco@kyokugen.org')->first()->state;

        $this->assertEquals('SP', $employeeNewState);
    }

    public function test_it_cannot_import_csv_with_invalid_date()
    {
        $csv = $this->getFile('app/tests/invalid-date-csv.csv');

        $this->json('POST', route('employees.store.from.csv'), [
            'employees' => $csv,
        ])->assertStatus(406);
    }

    public function getFile(string $filePath): UploadedFile
    {
        $file = new File(storage_path($filePath));

        $originalName = $file->getFilename();
        $mimeType = $file->getMimeType();
        $size = $file->getSize();

        return new UploadedFile($file->getRealPath(), $originalName, $mimeType, $size, true, true);
    }
}
