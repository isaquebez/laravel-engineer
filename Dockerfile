FROM php:7.4-fpm-alpine

RUN apk add --no-cache openssl bash shadow sqlite-dev
RUN docker-php-ext-install pdo pdo_sqlite

WORKDIR /var/www
RUN rm -rf /var/www/html

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN chown -R www-data:www-data /var/www
RUN ln -s public html
RUN usermod -u 1000 www-data

USER www-data

EXPOSE 9000
ENTRYPOINT ["php-fpm"]
