#!/bin/bash

composer install

mkdir .docker/sqlite && touch .docker/sqlite/database.sqlite \ &&
touch .docker/sqlite/database_testing.sqlite

cp .env.example .env
cp .env.testing.example .env.testing

php artisan config:cache
php artisan key:generate
php artisan queue:table
php artisan migrate:refresh --seed
php artisan passport:install

php-fpm
