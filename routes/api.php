<?php

use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\EmployeeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('user', [AuthController::class, 'user'])->name('login');

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('employees/store-from-csv', [EmployeeController::class, 'storeFromCsv'])
        ->name('employees.store.from.csv');
    Route::resource('employees', EmployeeController::class, ['except' => ['create', 'edit']]);
});
